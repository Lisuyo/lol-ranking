import os

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'league_ranking.views.home', name='home'),
    # url(r'^league_ranking/', include('league_ranking.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
#    url(r'^$', 'ranking.views.index'),
#    url(r'^$', 'index', name='index'),
    url(r'^eune$', 'ranking.views.eune'),
    url(r'^euw$', 'ranking.views.euw'),
    url(r'^add$', 'ranking.views.add'),
    url(r'^players_not_found$', 'ranking.views.missing_players'),



    (r'^league_ranking/site_media/(.*)$', 'django.views.static.serve', {'document_root': os.path.join(os.path.dirname(__file__), 'site_media')}),
)

urlpatterns += patterns('ranking.views',
    url(r'^$', 'index', name='index'))
