#-*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template.context import RequestContext

from ranking.models import Player, PlayerForm

def index(request):
    players = Player.objects.all().exclude(rating=None).order_by('-rating')
    return render_to_response('index.html', {'players' : players})



def eune(request):
    players = Player.objects.all().filter(server='eune').exclude(rating=None).order_by('-rating')
    return render_to_response('index.html', {'players' : players})


def euw(request):
    players = Player.objects.all().filter(server='euw').exclude(rating=None).order_by('-rating')
    return render_to_response('index.html', {'players' : players})


def missing_players(request):
    players = Player.objects.all().filter(rating=None)
    return render_to_response('missing.html', {'players' : players})

def add(request):
    if request.method == "POST":

        form = PlayerForm(request.POST)

        if form.is_valid():
            print request.POST
            message = 'Zostałeś dodany do rankingu. Ranking jest odświeżany przez bota raz dziennie'
            form.save()
        else:
            message = 'Coś poszło nie tak ;-)'


        return render_to_response('add.html',
                {'message': message},
            context_instance=RequestContext(request))
    else:
        return render_to_response('add.html',
                {'form': PlayerForm()},
            context_instance=RequestContext(request))