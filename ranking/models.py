from django.db import models
from django.forms.models import ModelForm

class Player(models.Model):
    user = models.CharField(max_length=30)
    wins = models.IntegerField(blank=True)
    losses = models.IntegerField(blank=True)
    rating = models.IntegerField(blank=True)
    rank = models.IntegerField(blank=True)
    nw_alias = models.CharField(max_length=50)

    servers = (
        ('EUNE', 'EUNE'),
        ('EUW', 'EUW')
    )
    server = models.CharField(max_length=10, choices=servers, default=servers[0])

class PlayerForm(ModelForm):
    class Meta:
        model = Player
        exclude = ('wins', 'losses', 'rating', 'rank')
